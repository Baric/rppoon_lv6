﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad3_4
{
    class BankCareTaker
    {
        private BankMemento previousState;

        public BankMemento GetPreviousState()
        {
            return previousState;
        }

        public void SetPreviousState(BankMemento value)
        {
            previousState = value;
        }
    }
}