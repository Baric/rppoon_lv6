﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Zad3_4
{
    class CareTaker
    {
        //public Memento PreviousState { get; set; }

        private List<Memento> PreviousState;
        private int i;

        public CareTaker()
        {
            this.PreviousState = new List<Memento>();
            i = PreviousState.Count - 1;
        }

        public CareTaker(List<Memento> PreviousState)
        {
            this.PreviousState = PreviousState;
            i = PreviousState.Count - 1;
        }

        public Memento Undo()
        {
            i -= i;
            if (i < 0) return null; { return PreviousState[i]; }
        }

        public void AddPreviousState(Memento state)
        {
            PreviousState.Add(state);
            i = PreviousState.Count - 1;
        }

        public Memento Redo()
        {
            i += i;
            if (i > PreviousState.Count - 1) { return null; }

                return PreviousState[i];
        }
    }
}
