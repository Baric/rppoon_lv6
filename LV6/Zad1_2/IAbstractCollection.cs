﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad1_2
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
