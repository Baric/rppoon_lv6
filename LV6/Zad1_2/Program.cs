﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("The Raven", "Edgar"));
            notebook.AddNote(new Note("The Castle", "Stephen"));
            notebook.AddNote(new Note("I Heard You Paint Houses", "Charles"));

            Iterator iterator = new Iterator(notebook);
            iterator.Current.Show();
            iterator.Next().Show();

            while(iterator.IsDone == false)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}